package me.koenn.gravestones.grave;

import me.koenn.gravestones.Gravestones;
import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hologram {

    private ArrayList<ArmorStand> armorStands = new ArrayList<>();
    private Grave grave;
    private int task;

    /**
     * Constructor for a new Hologram.
     * Automaticly starts the update timer.
     *
     * @param location Location of the Hologram
     * @param grave    Grave linked to the Hologram
     */
    public Hologram(Location location, Grave grave) {
        //Make sure the Location is not null.
        if (location == null) {
            return;
        }

        //Set local and instance variables.
        this.grave = grave;
        String minutes = String.valueOf(this.grave.getCounter().getTimeLeft() / 60);
        String seconds = String.valueOf(this.grave.getCounter().getTimeLeft() % 60);
        Location localLoc;

        //Set the time to infinite if the timeleft is -1.
        if (this.grave.getCounter().getTimeLeft() == -1) {
            minutes = "**";
            seconds = "**";
        }

        //Make the variables final.
        final String min = minutes;
        final String sec = seconds;

        //Get config settings and update the location.
        List<String> lines = ConfigManager.getList("hologramLayout", "graveSettings");
        localLoc = location.add(0, (lines.size() * 0.3), 0);

        //Make and save all ArmorStands.
        armorStands.addAll(lines.stream().map(line -> createStand(localLoc.add(0, -0.3, 0), format(line, min, sec))).collect(Collectors.toList()));

        //Start the updateTime task.
        this.task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Gravestones.getInstance(), this::updateTime, 2, 20);
    }

    /**
     * Update the expire time on the hologram.
     */
    private void updateTime() {
        //Make the minutes and seconds variables.
        String minutes = String.valueOf(this.grave.getCounter().getTimeLeft() / 60);
        String seconds = String.valueOf(this.grave.getCounter().getTimeLeft() % 60);

        //Set the time to infinite if the timeleft is -1.
        if (this.grave.getCounter().getTimeLeft() == -1) {
            minutes = "**";
            seconds = "**";
        }

        //Update the ArmorStand containing the {minutes} and {seconds} placeholders.
        armorStands.get(Util.getTimeFormatLine()).setCustomName(format(Util.getTimeLine(), minutes, seconds));
    }

    /**
     * Create an ArmorStand for one line of the Hologram.
     *
     * @param location Location of the ArmorStand
     * @param name     Name of the ArmorStand
     * @return ArmorStand object instance
     */
    private ArmorStand createStand(Location location, String name) {
        //Spawn the ArmorStand at the Location.
        ArmorStand stand = (ArmorStand) location.getWorld().spawnEntity(location.clone().add(0, -0.6, 0), EntityType.ARMOR_STAND);

        //Set the velocity to zero.
        stand.setVelocity(stand.getVelocity().zero());

        //Disable gravity and make the ArmorStand invisible.
        stand.setGravity(false);
        stand.setVisible(false);

        //Set the name of the ArmorStand and make the name visible.
        stand.setCustomName(name);
        stand.setCustomNameVisible(true);

        //Return the ArmorStand.
        return stand;
    }

    /**
     * Remove the Hologram, and cancel the update task.
     */
    public void remove() {
        //Stop the update task.
        Bukkit.getScheduler().cancelTask(task);

        //Remove all the ArmorStands.
        this.armorStands.forEach(ArmorStand::remove);
    }

    /**
     * Format a String by replace all placeholders.
     *
     * @param string  String to format
     * @param minutes Minutes left on the Counter
     * @param seconds Seconds left on the Counter
     * @return Formatted String
     */
    private String format(String string, String minutes, String seconds) {
        return string
                .replace("{player}", grave.getPlayerName())
                .replace("{minutes}", minutes)
                .replace("{seconds}", seconds);
    }
}
