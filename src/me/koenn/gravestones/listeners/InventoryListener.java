package me.koenn.gravestones.listeners;

import me.koenn.gravestones.Gravestones;
import me.koenn.gravestones.grave.Grave;
import me.koenn.gravestones.util.Util;
import me.koenn.updater.client.Updater;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.Set;

public class InventoryListener implements Listener {

    /**
     * Listener for the InventoryCloseEvent.
     *
     * @param event InventoryCloseEvent instance
     */
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        try {
            //Set local variables.
            Player player = (Player) event.getPlayer();
            Location block = player.getTargetBlock((Set<Material>) null, 200).getLocation();

            //Loop through all Gravestones.
            for (Grave grave : Grave.gravestones) {

                //Make sure the grave has location.
                if (!grave.getClickLocs().isEmpty()) {

                    //Loop through the grave locations.
                    for (Location location : grave.getClickLocs()) {

                        //Make sure the location is not null.
                        if (location != null) {

                            //Check if the locations match.
                            if (location.getBlockX() == block.getBlockX() && location.getBlockY() == block.getBlockY() && location.getBlockZ() == block.getBlockZ()) {

                                //Check if the Inventory is empty.
                                if (Util.isEmpty(event.getView().getTopInventory())) {

                                    //Destroy the grave.
                                    Bukkit.getScheduler().scheduleSyncDelayedTask(Gravestones.getInstance(), grave::destroy, 10);

                                    //Stop all loops.
                                    return;
                                }

                                //Set the Grave's state to closed.
                                grave.setOpen(false);

                                //Stop all loops.
                                return;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            //Report error to server.
            Updater.getUpdater().sendError(ex);
        }
    }
}
