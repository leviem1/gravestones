package me.koenn.gravestones;

import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.grave.Grave;
import me.koenn.gravestones.listeners.*;
import me.koenn.updater.client.Updater;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;

/**
 * Gravestones plugin.
 *
 * @author Koenn
 */
public class Gravestones extends JavaPlugin {

    /**
     * Static instance of this plugin class.
     */
    private static Plugin instance;

    /**
     * Bukkit version. Can be either 1.8, 1.9, 1.10 or 1.11
     */
    private static double bukkitVersion;

    /**
     * Get the version of Bukkit that's on the server.
     *
     * @return double version
     */
    public static double getBukkitVersion() {
        return bukkitVersion;
    }

    /**
     * Get the instance of this class.
     *
     * @return Plugin instance
     */
    public static Plugin getInstance() {
        return instance;
    }

    /**
     * Simple logger method to log a message to the console.
     *
     * @param msg Message to log
     */
    private void log(String msg) {
        getLogger().info(msg);
    }

    /**
     * Plugin enable method.
     * Gets called when the plugin enables
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void onEnable() {
        try {
            //Set static variables.
            instance = this;

            //Send credit message.
            log("All credits for this plugin go to Koenn");

            //Check the version and set the bukkitVersion variable.
            String versionString = Bukkit.getServer().getBukkitVersion();
            if (versionString.contains("1.9")) {
                bukkitVersion = 1.9;
            } else if (versionString.contains("1.8")) {
                bukkitVersion = 1.8;
            } else if (versionString.contains("1.10")) {
                bukkitVersion = 1.10;
            } else if (versionString.contains("1.11")) {
                bukkitVersion = 1.11;
            } else {
                log("###########################################################");
                log("This plugin is only compatible with 1.8, 1.9, 1.10 and 1.11");
                log("###########################################################");
                Bukkit.getPluginManager().disablePlugin(this);
                return;
            }
            log("Loading plugin for minecraft version " + versionString + "!");

            //Create datafolder.
            if (!this.getDataFolder().exists()) {
                this.getDataFolder().mkdir();
            }

            //Setup the ConfigManager.
            ConfigManager.setup(this);

            //Save the gravestone.schematic to the datafolder.
            if (!new File(getDataFolder(), "gravestone.schematic").exists()) {
                saveResource("gravestone.schematic", false);
            }

            //Save the gravestone_no_gravity.schematic to the datafolder.
            if (!new File(getDataFolder(), "gravestone_no_gravity.schematic").exists()) {
                saveResource("gravestone_no_gravity.schematic", false);
            }

            //Register all Bukkit event Listeners.
            registerListener(new DeathListener());
            registerListener(new ClickListener());
            registerListener(new InventoryListener());
            registerListener(new BreakListener());
            registerListener(new ChunkListener());

            //Start the Updater.
            if (Boolean.parseBoolean(ConfigManager.getString("autoUpdate", "graveSettings").toUpperCase())) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> new Updater(this, "https://www.spigotmc.org/resources/gravestones.20545/", "20545"), 20);
            }
        } catch (Exception ex) {
            //Report error to server.
            new Updater(this).sendError(ex);
        }
    }

    /**
     * Register a Bukkit event Listener.
     *
     * @param listener Listener instance
     */
    private void registerListener(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, this);
    }

    /**
     * Plugin disable method.
     * Gets called when the plugin disables.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void onDisable() {
        try {
            //Check if there are any active graves.
            if (!Grave.gravestones.isEmpty()) {

                //Try to destroy all active graves.
                ((ArrayList<Grave>) Grave.gravestones.clone()).forEach(grave -> {
                    if (grave != null) {
                        grave.destroy();
                    } else {
                        this.getLogger().warning("It seems like an error occurred while trying to remove an active gravestone. Please contact the plugin developer if you keep getting this error!");
                    }
                });
            }

            //Send credit message.
            log("All credits for this plugin go to Koenn");
        } catch (Exception ex) {
            //Report error to server.
            new Updater(this).sendError(ex);
        }
    }
}