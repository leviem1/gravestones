## **Are you scared of losing all your stuff in lava? Is your server lagging because of too many items on the ground? Then this is the perfect plugin for you!** ##

![Screenshot grave.png](https://proxy.spigotmc.org/f8e70e06131c3d8ddc109cc07c45e437eb2aaf2f?url=http%3A%2F%2Fi.imgur.com%2FXF6SAwc.png)

With this plugin installed, as soon as you die, a grave will spawn, containing all of your items. It will send you a message, telling you where you can find back your grave. 
After a set amount of time, the grave will expire and your items will be gone. You have the options to automaticly break the gravel in the grave, or just leave them.

When you try to open your grave there's a chance a zombie will spawn! This zombie is immune to fire (So he won't burn in the sun) and he will try to kill you. (This is all configurable)


If you want to modify how your gravestone looks, take a look [here](https://bitbucket.org/Koennn/gravestones/wiki/Modify%20Gravestone).


Please report any issues or bugs you find, I will try to fix them as soon as possible!

**[Download the plugin now!](https://www.spigotmc.org/resources/gravestones.20545)**